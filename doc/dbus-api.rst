=========
D-Bus API
=========
dLeyna is a collection of D-Bus services and APIs that try to ease the development of different
DLNA use-cases. The following documents describe the D-Bus API that is provided for UPnP or DLNA
MediaServers and MediaRenderers

A couple of examples can be found in the `examples <https://gitlab.gnome.org/World/dLeyna/-/tree/master/examples>`_
folder or the `python modules <https://gitlab.gnome.org/World/dLeyna/-/tree/master/python/dLeyna>`_
folder using C and Python to interact with dLeyna services.

.. toctree::
   :maxdepth: 3
   :caption: Contents

   server/dbus
   renderer/dbus


