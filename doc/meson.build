rst2man = find_program('rst2man', 'rst2man.py',
  required: get_option('man_pages'),
  disabler: true
)

custom_target(
    'renderer conf manpages',
    input: 'dleyna-renderer-service.conf.rst',
    output: 'dleyna-renderer-service.conf.5',
    command: [rst2man, '@INPUT@', '@OUTPUT@'],
    install: true,
    install_dir: join_paths(get_option('mandir'), 'man5')
)

custom_target(
    'renderer service manpages',
    input: 'dleyna-renderer-service.rst',
    output: 'dleyna-renderer-service.1',
    command: [rst2man, '@INPUT@', '@OUTPUT@'],
    install: true,
    install_dir: join_paths(get_option('mandir'), 'man1')
)

custom_target(
    'server conf manpages',
    input: 'dleyna-server-service.conf.rst',
    output: 'dleyna-server-service.conf.5',
    command: [rst2man, '@INPUT@', '@OUTPUT@'],
    install: true,
    install_dir: join_paths(get_option('mandir'), 'man5')
)

custom_target(
    'server service manpages',
    input: 'dleyna-server-service.rst',
    output: 'dleyna-server-service.1',
    command: [rst2man, '@INPUT@', '@OUTPUT@'],
    install: true,
    install_dir: join_paths(get_option('mandir'), 'man1')
)


sphinx = find_program('sphinx-build', required: get_option('docs'), disabler: true)

docs_rst = [
  'server/dbus.rst',
  'server/manager.rst',
  'server/server.rst',
  'server/item.rst',
  'renderer/dbus.rst',
  'renderer/manager.rst',
  'renderer/renderer.rst',
  'index.rst',
  'dbus-api.rst',
  'references.rst',
  'glossary.rst',
  'man-pages.rst',
  'logging.rst',
  'dleyna-renderer-service.conf.rst',
  'dleyna-renderer-service.rst',
  'dleyna-server-service.conf.rst',
  'dleyna-server-service.rst'
]

custom_target('docs',
  input: docs_rst,
  depend_files: files('conf.py'),
  output: 'docs',
  command: [
    sphinx,
    '-n', '-b', 'html', '-q',
    '-Drelease=' + meson.project_version(),
    meson.current_source_dir(),
    '@OUTPUT@'])
