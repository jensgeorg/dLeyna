==================
The Manager Object
==================

There is only ever a single instance of this object.  The manager
object exposes two D-Bus interfaces:

.. contents::

1. com.intel.dLeynaServer.Manager
2. org.freedesktop.DBus.Properties

******************************
com.intel.dLeynaServer.Manager
******************************

Methods
=======

The interface com.intel.dLeynaServer.Manager contains six methods.
Descriptions of each of these methods along with their D-Bus
signatures are given below.

GetServers() -> ao
------------------

    GetServers takes no parameters and returns an array of D-Bus object
    paths.  Each of these paths reference a D-Bus object that represents a
    single DMS.

GetVersion() -> s
-----------------

    Returns the version number of dleyna-server-service

Release()
---------

    Indicates to dleyna-server-service that a client is no longer interested
    in its services.  Internally, dleyna-server-service maintains a reference
    count.  This reference count is increased when a new client connects.
    It is decreased when a client quits.  When the reference count reaches
    0, dleyna-server-service exits.  A call to Release also decreases the
    reference count.  Clients should call this method if they intend to
    keep running but they have no immediate plans to invoke any of
    dleyna-server-service's methods. This allows dleyna-server-service to quit,
    freeing up system resources.

SetProtocolInfo(s ProtocolInfo) -> void
---------------------------------------

    Informs dleyna-server-service of the mime types and network protocols
    supported by the client.

    The explanation behind this function is a little complex.  DMSs often
    support a feature call transcoding.  This allows them to publish each
    media item they manage in a number of different formats.  This is
    useful as not all devices support the same set of media formats and
    codecs.  For example, a DMS might publish two separate URLS for a
    video file.  The first URL might point to the file in its original
    format, e.g., to an ogg vorbis file.  The second URL however, could
    point to a MP4 encoded version of the same file.  The second URL might
    be preferable if viewing the file on a mobile device.  In short,
    SetProtocolInfo allows clients to specify the formats that they would
    like to receive media content in.  This function should be called by
    all MediaPlayers.  Doing so, will ensure that dleyna-server-service only
    returns compatible URLs via the org.gnome.MediaItem2 interface.  For
    more information see the section on MediaServer2Spec below.

    The Protocol info field is a comma separated list of protocol info
    values.  Each protocol info value consists of 4 fields separated by
    colons.  Unfortunately, the format is too complex to describe in this
    document.  The reader is referred to the UPnP Connection Manager
    Service Template document [CM2]_ and the DLNA Guidelines [DLNA]_ where it is
    described extensively.  However, an example protocol info value is
    presented below, to give the reader an idea of what such a string
    might look like.

    "http-get:*:audio/mp4:DLNA.ORG_PN=AMR_WBplus,http-get:*:image/jpeg:DLNA.ORG_PN=JPEG_MED"

    The protocol info value above indicates that the client supports the
    retrieval, via HTTP, and the playback of audio MP4 and JPEG files.

PreferLocalAddresses(b Prefer) -> void
--------------------------------------

    Indicates whether  local addresses should be prioritized  or not. DMSs
    tend to make their services available on multiple interfaces. If a DMP
    and a DMS are running on the same device, the DMP will have the option
    of  communicating with  the  DMS over  the  loopback or  the LAN.   By
    default dleyna-server-service  will return loopback  addresses to clients
    running on  the same device.   This is not  very helpful for  DMCs who
    need  non-local  addresses  to  pass  to renderers  running  on  other
    devices.   DMCs should  therefore call  this function  with  the value
    FALSE before requesting any URLs from any servers.

Rescan() -> void
----------------

    Forces a rescan for DMSs on the local area network.  This is useful to
    detect DMSs which have shut down without sending BYE messages or to
    discover new DMSs which for some reason were not detected when either
    they, or the device on which dLeyna-server runs, was started or joined
    the network.  New in version 0.0.2.


Properties
==========

The com.intel.dLeynaServer.Manager interface exposes information via a number
of D-Bus properties.  These properties are described below:

+-------------------+-----------+-----------+-----------------------------------------+
|     Name          |   Type    |m/o [#f1]_ |              Description                |
+===================+===========+===========+=========================================+
| NeverQuit         |     b     | m         | True if the service always stay in      |
|                   |           |           | memory running.  False if the service   |
|                   |           |           | quit when the last client disconnects   |
+-------------------+-----------+-----------+-----------------------------------------+
| WhiteListEntries  |     as    | m         | The list of entries that compose the    |
|                   |           |           | white list used to filter the networks. |
|                   |           |           | An Entry could be an interface name     |
|                   |           |           | (eth0), an ip address (127.0.0.1) or    |
|                   |           |           | a SSID (MyWiFi)                         |
+-------------------+-----------+-----------+-----------------------------------------+
| WhiteListEnabled  |     b     | m         | True if the Network Filtering is active |
+-------------------+-----------+-----------+-----------------------------------------+

A ``org.freedesktop.DBus.Properties.PropertiesChanged`` signal is emitted when
these properties change. These properties can be changed using the ``Set()`` method of
``org.freedesktop.DBus.Properties`` interface.

Signals
=======

The ``com.intel.dLeynaServer.Manager`` interface also exposes two signals.

FoundServer(o)
--------------

    Is generated whenever a new DMS is detected on the local area network.
    The signal contains the path of the newly discovered server.

LostServer(o)
-------------

    Is generated whenever a DMS is shutdown.  The signal contains the path
    of the server which has just been shutdown.

.. rubric:: Footnotes

.. [#f1] m/o indicates whether the property is optional or mandatory.
