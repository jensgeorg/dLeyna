=====================
The Container Objects
=====================

Container objects represent folders in a DMS.  In order to manipulate
a container object one first needs to discover its path.  This can be
done by calling one of the List or Search methods implemented by the
server object or another container object.  Note that a server object
is also a container object so a container object can be constructed by
using a server's D-Bus object path.

Container objects support two interfaces: org.gnome.MediaObject2 and
org.gnome.MediaContainer2.  Both of these interfaces have been
described in detail in :ref:`the server object<the_server_objects>`.

An example of how container objects can be used is given in the
following function.

.. code:: python

        def tree(server_path, level=0):
            bus = dbus.SessionBus()
            container = dbus.Interface(bus.get_object(
                        'com.intel.dleyna-server', server_path),
                                                'org.gnome.UPnP.MediaContainer2')
            objects = container.ListChildren(0, 0, ["DisplayName", "Path", "Type"])
            for props in objects:
                print " " * (level * 4) + ( props["DisplayName"] +
                                            " : (" + props["Path"]+ ")")
                if props["Type"] == "container":
                    tree(props["Path"], level + 1)

When given the path of a container, and this could be a server object
which acts as a root container, it recursively prints the contents of
that container in depth first order.  For example, to dump the entire
contents of the My Media server introduced above, one would simply
need to invoke tree("/com/intel/dLeynaServer/server/3").


