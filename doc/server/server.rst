.. _the_server_objects:

==================
The Server Objects
==================

dleyna-server-service exposes a separate D-Bus object for each DMS it
detects on the LAN.  These objects serve two purposes.

1. They allow the client to retrieve information about the DMS, such
as its name, the URL of its icon, its manufacturer, etc.
2. They represent the root container of the DMS allowing clients to
search and browse the DMSs contents.

Each server object exposes three separate interfaces:

1. com.intel.dLeynaServer.MediaDevice
2. org.gnome.MediaObject2
3. org.gnome.UPnP.MediaContainer2


**********************************
com.intel.dLeynaServer.MediaDevice
**********************************

Methods
=======

The com.intel.dLeynaServer.MediaDevice interface currently exposes 8 methods:

UploadToAnyContainer(s DisplayName, s FilePath) -> (u UploadId, o ObjectPath)
-----------------------------------------------------------------------------

    UploadToAnyContainer allows a client to upload a local file to a DMS.
    The DMS chooses the best place to store the file based on the file's
    type.  DisplayName is the friendly name the DMS should associate with
    the file and FilePath is a full path to the file.  The successful
    completion of this method indicates that the upload has begun, but has
    not necessarily finished.  There are two return values.  The UploadId,
    which can be used to monitor the progress of the upload and to
    cancel it and ObjectPath, which contains the path of the newly created object.

CreateContainerInAnyContainer(s DisplayName, s TypeEx, as ChildTypes) -> o ObjectPath
-------------------------------------------------------------------------------------

    Creates a new container. The DMS chooses the best place to create
    this new container. DisplayName is the name of the new container,
    TypeEx is the extended type and ChildTypes is an array of extended types that
    can be stored in the new folder, e.g., ['video','container'].
    A special value of ['*'] indicates that no restriction is placed
    on the types of objects that can be stored in the container.
    The path of the newly created object is returned.

GetUploadStatus(u UploadId) -> (s UploadStatus, t Length, t Total)
------------------------------------------------------------------

    GetUploadStatus returns the current status of an upload previously
    started by a call to Upload or UploadToAnyContainer.  Clients should
    pass in the UploadId they received as a return value from either
    of these two functions.  This method returns three pieces of information.

    1) UploadStatus, indicating the status of the upload.  Four separate values are possible.  "IN_PROGRESS", "CANCELLED", "ERROR", "COMPLETED"
    2)  Length, indicating the number of bytes that have been transferred so far.
    3) Total, indicating the total size of the upload.

    Clients can call GetUploadStatus to retrieve the status of an upload up to
    30 seconds after the specified upload has finished.

    The only exception to this rule is if the DMS to which the file is being
    uploaded shuts down or disappears from the UPnP network.  If this happens
    all active uploads to this DMS will be cancelled and the Device object
    representing this DMS will disappear.  As the Device object is no longer
    available, clients cannot call GetUploadStatus to determine the status
    of their uploads.  Clients that initiate uploads should listen for
    the LostServer signal.  They should assume that any uploads to a DMS that
    were in progress when this signal is received for that DMS, have been
    cancelled.

GetUploadIDs() -> (au UploadIDs)
--------------------------------

    GetUploadIDs returns an array containing the IDs of all the outstanding upload
    tasks.  An empty array will be returned if no uploads are in progress on the
    current device.  As with GetUploadStatus, the IDs of upload tasks will be
    returned by GetUploadIDs for 30 seconds after the uploads have finished.

CancelUpload(u UploadId) -> void
--------------------------------

    Cancels the upload identified by UploadId.  This function has no effect if the
    upload has already completed, i.e., its status is set to COMPLETED.

Cancel() -> void
----------------

    Cancels all requests a client has outstanding on that server.

GetIcon(s RequestedMimeType, s Resolution) -> (ay Bytes, s MimeType)
--------------------------------------------------------------------

    Returns the device icon bytes and mime type according to the
    RequestedMimeType and Resolution parameters.

    Both, RequestedMimeType and Resolution parameters, are currently
    reserved for future use and should be set as an empty string.

BrowseObjects(ao ObjectPath, as Filter) -> aa{sv}
-------------------------------------------------

    This method returns properties of all media objects specified by the first
    parameter. The list of properties to return is specified by the second
    parameter. The format and the behaviour of this array is identical to the
    filter argument passed to the Search and List functions.

    The result is an array of property name+value dictionaries, one for each object
    specified in the first argument.

    In case of even a single 'Invalid path' in ObjectPath parameter, the function
    will fail and return an error.

    In case of 'Invalid ID', the function will succeed, and an 'Error' property
    will be set for this object.

    We guarantee that the number of elements of the result array is the same and in
    the same order as the ObjectPath array.

    The purposes of this method are:

    1. It allows applications to easily retrieve information about unrelated objects in one call.
    2. It should be used in preference to GetAll, if an application wishes to retrieve a subset of an object's properties. Browse is likely to be more efficient than GetAll in such cases.

Wake() -> void
--------------

    Sends a magic packet to the server to wake it up if it is in sleeping state.

Properties
==========

The com.intel.dLeynaServer.MediaDevice interface exposes information about the
DMS via a number of D-Bus properties.  These properties are described
below:

+-------------------+-----------+-----------+---------------------------------------------+
|     Name          |   Type    |m/o [#f1]_ |              Description                    |
+===================+===========+===========+=============================================+
| DeviceType        |     s     | m         | The UPnP type of the device, e.g.,          |
|                   |           |           | urn:schemas-upnp-org:device:MediaServer:1   |
+-------------------+-----------+-----------+---------------------------------------------+
| UDN               |     s     | m         | The Unique Device Name of the server.       |
+-------------------+-----------+-----------+---------------------------------------------+
| RootUDN           |     s     | o         | The Unique Device Name of the root          |
|                   |           |           | device, if the server is a sub-device.      |
+-------------------+-----------+-----------+---------------------------------------------+
| FriendlyName      |     s     | m         | The friendly name of the media server.      |
+-------------------+-----------+-----------+---------------------------------------------+
| IconURL           |     s     | o         | A URL pointing to an icon that              |
|                   |           |           | graphically identifies the server           |
+-------------------+-----------+-----------+---------------------------------------------+
| Manufacturer      |     s     | m         | A string identifying the manufacturer       |
|                   |           |           | of the server                               |
+-------------------+-----------+-----------+---------------------------------------------+
| ManufacturerUrl   |     s     | o         | A URL pointing to the manufacturer's        |
|                   |           |           | web site.                                   |
+-------------------+-----------+-----------+---------------------------------------------+
| ModelDescription  |     s     | o         | A description of the server.                |
+-------------------+-----------+-----------+---------------------------------------------+
| ModelName         |     s     | m         | The model name of the server.               |
+-------------------+-----------+-----------+---------------------------------------------+
| ModelNumber       |     s     | o         | The server's version number                 |
+-------------------+-----------+-----------+---------------------------------------------+
| SerialNumber      |     s     | o         | The server's serial number                  |
+-------------------+-----------+-----------+---------------------------------------------+
| PresentationURL   |     s     | o         | The presentation URL of the server,         |
|                   |           |           | i.e., a link to it's HTML management        |
|                   |           |           | interface.                                  |
+-------------------+-----------+-----------+---------------------------------------------+
| DLNACaps          |   a{sv}   | o         | Represents the device capabilities as       |
|                   |           |           | announced in the device description         |
|                   |           |           | file via the dlna:X_DLNACAP element. [#f2]_ |
+-------------------+-----------+-----------+---------------------------------------------+
| SearchCaps        |     as    | m         | List of property names that can be used     |
|                   |           |           | in search queries.                          |
|                   |           |           | A wildcard (“*”) indicates that the         |
|                   |           |           | device supports search queries using        |
|                   |           |           | any property name(s).                       |
|                   |           |           | Empty if not supported by the device.       |
+-------------------+-----------+-----------+---------------------------------------------+
| SortCaps          |     as    | m         | List of property names that can be used     |
|                   |           |           | to sort Search() or Browse() action         |
|                   |           |           | results.                                    |
|                   |           |           | A wildcard (“*”) indicates that the         |
|                   |           |           | device supports sorting using all           |
|                   |           |           | property names.                             |
|                   |           |           | Empty if not supported by the device.       |
+-------------------+-----------+-----------+---------------------------------------------+
| SortExtCaps       |     as    | o         | List of sort modifiers that can be used     |
|                   |           |           | to sort Search() or Browse() results.       |
|                   |           |           | Empty if not supported by the device.       |
+-------------------+-----------+-----------+---------------------------------------------+
| FeatureList       |  a(ssao)  | o         | Each element in the FeatureList array       |
|                   |           |           | represents a feature supported by the       |
|                   |           |           | DMS. Each feature contains three pieces     |
|                   |           |           | of information, a name, a version number    |
|                   |           |           | and an array of object paths that can       |
|                   |           |           | clients can use to take advantage of the    |
|                   |           |           | feature. There are three standardized       |
|                   |           |           | feature names, BOOKMARK, EPG and TUNER.     |
+-------------------+-----------+-----------+---------------------------------------------+
| ServiceResetToken |     s     | o         |  A string containing the current            |
|                   |           |           |  service reset token of the server.         |
+-------------------+-----------+-----------+---------------------------------------------+
| SystemUpdateID    |     u     | m         | An integer value that is incremenented      |
|                   |           |           | every time changes are made to the DMS.     |
+-------------------+-----------+-----------+---------------------------------------------+
| Sleeping          |     b     | o         | An boolean value which represents the       |
|                   |           |           | server sleeping state.                      |
+-------------------+-----------+-----------+---------------------------------------------+

All of the above properties are static with the sole exception
of SystemUpdateID. A org.freedesktop.DBus.Properties.PropertiesChanged signal
is emitted when this property changes.


Signals
-------

The com.intel.dLeynaServer.MediaDevice interface also exposes three signals.

Changed (aa{sv} ChangedObjects)
-------------------------------

    The Changed signal is generated by servers to provide precise information
    of changes that have taken place to their content.  Changed is useful for
    synchronisation controllers.  It allows them to dynamically update their local
    cache of the server's content. This signal contains an array of zero or more
    dictionary elements, each of which represents a change to a specific object.
    The dictionary is composed of the following keys:

+----------------+------+------------+------------------------------------------------+
|      Key       | Type | m/o [#f1]_ |                 Description                    |
+================+======+============+================================================+
| ChangeType     |  u   |  m         | Contains the Type of the notification:         |
|                |      |            |   - (ADD) an object has been added             |
|                |      |            |   - (MOD) an object has been modified          |
|                |      |            |   - (DEL) an object has been deleted           |
|                |      |            |   - (DONE) an update to a sub-tree has         |
|                |      |            |     completed                                  |
|                |      |            |   - (CONTAINER) a container has been updated   |
|                |      |            |                                                |
+----------------+------+------------+------------------------------------------------+
| Path           |  o   |  m         | Contains the Object Path property of the       |
|                |      |            | object that was changed.                       |
+----------------+------+------------+------------------------------------------------+
| UpdateID       |  u   |  m         | ContainerUpdateID or SystemUpdateID when the   |
|                |      |            | object was changed.                            |
+----------------+------+------------+------------------------------------------------+
| SubTreeUpdate  |  b   |  o         | True if the object was added as part of a      |
|                |      |            | subtree update.                                |
+----------------+------+------------+------------------------------------------------+
| Parent         |  o   |  o         | Contains the Object Path property of the       |
|                |      |            | parent container of the changed object.        |
+----------------+------+------------+------------------------------------------------+
| TypeEx         |  s   |  o         | Contains the value of the TypeEx property      |
|                |      |            | (converted upnp:class property) of the         |
|                |      |            | object that was changed.                       |
+----------------+------+------------+------------------------------------------------+
| Type           |  s   |  o         | Contains the value of the Type property        |
|                |      |            | (converted upnp:class property) of the object  |
|                |      |            | that was changed.                              |
+----------------+------+------------+------------------------------------------------+

ContainerUpdateIDs(a(ou) ContainerPathsIDs)
-------------------------------------------

    Is generated when the contents of one or more folders maintained by the
    server have changed. This signal contains an array of paths/ContainerUpdateID
    of the server containers that have changed.

UploadUpdate(u UploadId, s UploadStatus, Length t, Total t)
-----------------------------------------------------------

    Is generated when an upload completes, fails or is cancelled.  The first
    parameter is the ID of the upload.   The second contains one of three values,
    "CANCELLED", "COMPLETED", "ERROR", indicating whether the upload was cancelled,
    completed successfully or failed, respectively.  The third parameter indicates
    the total amount of bytes that were uploaded and the fourth, the size of the
    file being uploaded.

Examples
========

Here is some example code in python that enumerates all the media
servers present on the network and prints their names and the paths of
the D-Bus objects that represent them, to the screen.

.. code:: python

  import dbus

  bus = dbus.SessionBus()
  manager = dbus.Interface(bus.get_object('com.intel.dleyna-server',
                                          '/com/intel/dLeynaServer'),
                           'com.intel.dLeynaServer.Manager')
  for path in manager.GetServers():
      server = dbus.Interface(bus.get_object(
              'com.intel.dleyna-server', path),
                              'org.freedesktop.DBus.Properties')
      folderName = server.Get("", "DisplayName")
      print '{0:<30}{1:<30}'.format(folderName , path)


Running this code on a LAN with 3 DMSs produces the following output::

    My Media                      /com/intel/dLeynaServer/server/3
    Root                          /com/intel/dLeynaServer/server/1
    Test Streams                  /com/intel/dLeynaServer/server/4


**********************
org.gnome.MediaObject2
**********************

Additional Methods
==================

Delete() -> void
----------------

    Delete will simply call the UPnP DestroyObject method on the relevant
    server side object. Note that calling Delete on the root object of a
    server will delete all the contents on the server but not the
    device object itself.

Update(a{sv} ToAddUpdate, as ToDelete) -> void
----------------------------------------------

    Updates the meta data of an existing DMS object.  It is based on the
    MediaServerAv UpdateObject command.  ToAddUpdate is a dictionary of
    property name value pairs that are to be updated, or added if they do
    not already exist.  ToDelete is an array of properties to be deleted.
    The same property cannot appear in both ToAddUpdate and ToDelete.
    Only the following properties can be updated: 'Date', 'DisplayName',
    'Artists', 'Album', 'TypeEx', 'TrackNumber'.

GetMetaData() -> s
------------------

    Returns the object meta data information in DIDL-Lite XML format.  This
    is useful when writing Digital Media Controllers. The controllers can
    pass this information to the DMR which can use it to determine the server
    side streaming options for the object. New in v0.0.2.

Additional Values for the Type Property
=======================================

The org.gnome.MediaObject2 interface exposes some basic properties of
the server's root directory.  This interface is described
MediaServer2Spec specification [MS2S]_.  dleyna-server-service enhances this
interface by adding new properties and methods and by defining
additional values for the Type property.

The MediaServer2Spec Type property can be set to one of only seven
possible values:  'container', 'video', 'video.movie', 'audio',
'music', 'image' or 'image.photo'.

This causes a number of problems for dLeyna-server.  First, there is
only one value for container, so it is not possible to indicate if the
container is an album or a playlist, for instance.  Secondly, there is
no way to represent an item that is not an audio, video or image item,
such as a generic item (UPnP class of object.item) or a playlist item.

dLeyna-server solves these two different problems in two ways:

- It provides a new property called TypeEx, which provides extended
  type information.  TypeEx is a superset of Type, providing specific
  values of object types that are not supported by Type.  For example,
  for an album, Type will be container but TypeEx will be set to
  container.album.musicAlbum.

- It provides a new catch all value for Type, item.unclassified.
  item.unclassified means that the object is an item but it is not
  an audio, video, or image item, e.g.,a  generic item or a playlist item.
  The actual type of the item is stored in TypeEx.  item.unclassified
  may be specified as a value for the Type property in searches.  In such
  cases it is treated as the UPnP type object.item.

Adding an additional value to MediaServer2Spec's Type property breaks our
compliance with this spec a little, but it was necessary to achieve UPnP
certification.

Additional Properties
=====================

+-----------------+-------+-------------+------------------------------------------+
|     Name        | Type  | m/o* [#f1]_ |              Description                 |
+=================+=======+=============+==========================================+
| DLNAManaged     | a{sb} |  o          |  A dictionary of boolean values          |
|                 |       |             |  indicating the values of the various    |
|                 |       |             |  DLNA managed attributes. There are 5    |
|                 |       |             |  keys: Upload, CreateContainer, Delete,  |
|                 |       |             |  UploadDelete, ChangeMeta                |
+-----------------+-------+-------------+------------------------------------------+
| Restricted      |  b    |  m          |  Indicates whether the object is         |
|                 |       |             |  modifiable.                             |
+-----------------+-------+-------------+------------------------------------------+
| Creator         |  s    |  o          |  Indicates an entity that owns the       |
|                 |       |             |  content or is primarily responsible for |
|                 |       |             |  creating the content.                   |
+-----------------+-------+-------------+------------------------------------------+
| ObjectUpdateID  |  u    |  o          |  Represents a last-modified timestamp    |
|                 |       |             |  for the object relative to the          |
|                 |       |             |  SystemUpdateID state variable.          |
+-----------------+-------+-------------+------------------------------------------+
| TypeEx          |  s    |  m          |  The extended Type of the object.        |
|                 |       |             |  TypeEx permits a superset of the values |
|                 |       |             |  supported by the Type property.  When   |
|                 |       |             |  the Type of an object is accurately     |
|                 |       |             |  described by Type, i.e., the object     |
|                 |       |             |  is a basic container or is an image,    |
|                 |       |             |  audio, or video file, TypeEx = Type.    |
|                 |       |             |  For objects where this is not the case, |
|                 |       |             |  e.g., an album, the TypeEx value is     |
|                 |       |             |  formed by removing the                  |
|                 |       |             |  "object." prefix from the UPnP          |
|                 |       |             |  class. For example, the TypeEx value    |
|                 |       |             |  for the UPnP class,                     |
|                 |       |             |  object.container.playlistContainer is   |
|                 |       |             |  container.playlistContainer.            |
|                 |       |             |                                          |
|                 |       |             |  New in version 0.2.0.                   |
+-----------------+-------+-------------+------------------------------------------+

*************************
org.gnome.MediaContainer2
*************************

The org.gnome.MediaContainer2 interface exposes some additional
properties of the root directory.  It also provides methods that can
be used by clients to perform a recursive search on the root directory
and to retrieve a list of its children.  The org.gnome.MediaContainer2
interface is documented in the MediaServer2Spec (3).  However,
dleyna-server-service's implementation of org.gnome.MediaContainer2
differs slightly from the specification.  These differences can be
grouped into two categories: unsupported properties and new methods.

Unsupported Properties
======================

The properties org.gnome.UPnP.MediaContainer2.ItemCount and
org.gnome.UPnP.MediaContainer2.ContainerCount are not implemented as
there is no way to efficiently implement these properties in
dleyna-server-service.

In addition, org.gnome.UPnP.MediaContainer2.Icon is not supported
either as its implementation is really complicated, requiring the
creation of virtual objects.  Furthermore, it cannot be properly
implemented in dleyna-server-service as the UPnP servers do not provide
us with enough information to populate the mandatory properties for
these virtual objects.  Nevertheless, clients can retrieve a URL that
points to a server's icon via the com.intel.dLeynaServer.MediaDevice property
IconURL.

Please note that none of these unsupported properties are mandatory,
so their absence does not affect dleyna-server-service's compatibility
with MediaServer2Spec.

Additional properties unsupported by org.gnome.MediaContainer2
==============================================================

+------------------------+------+------------+------------------------------------+
|     Name               | Type | m/o [#f1]_ |              Description           |
+========================+======+============+====================================+
| CreateClasses          |a(sb) |  o         |  The CreateClasses property is an  |
|                        |      |            |  array of tuples (sb) that lists   |
|                        |      |            |  the extended types of objects that|
|                        |      |            |  that can be created in a          |
|                        |      |            |  container. A boolean value is     |
|                        |      |            |  associated with each type.  This  |
|                        |      |            |  boolean indicates whether objects |
|                        |      |            |  of types derived from the         |
|                        |      |            |  specified extended type can also  |
|                        |      |            |  be created in this container.     |
|                        |      |            |  For example,                      |
|                        |      |            |  ("container.album.","true")       |
|                        |      |            |  would indicate that objects of    |
|                        |      |            |  type container.album,             |
|                        |      |            |  container.album.music and         |
|                        |      |            |  container.album.photo can be      |
|                        |      |            |  created in the container.         |
+------------------------+------+------------+------------------------------------+
| ContainerUpdateID      |  u   |  o         |  Contains the value of the         |
|                        |      |            |  SystemUpdateID state variable     |
|                        |      |            |  generated by the most recent      |
|                        |      |            |  container modification for that   |
|                        |      |            |  container                         |
+------------------------+------+------------+------------------------------------+
| TotalDeletedChildCount |  u   |  o         |  Contains the total number of      |
|                        |      |            |  child objects that have been      |
|                        |      |            |  deleted from a container object   |
|                        |      |            |  since the last initialization     |
+------------------------+------+------------+------------------------------------+
| Resources              |aa{sv}|  o         |  Returns the set of resources      |
|                        |      |            |  associated with a container.      |
|                        |      |            |  New in v0.0.2                     |
+------------------------+------+------------+------------------------------------+
| DLNAConversion         | a{sb}|  o         |  See "Container properties"        |
| DLNAFlags              | a{sb}|  o         |  paragraph below. New in v0.0.2    |
| DLNAOperation          | a{sb}|  o         |                                    |
| DLNAProfile            | s    |  o         |                                    |
| MIMEType               | s    |  o         |                                    |
| Size                   | x    |  o         |                                    |
| UpdateCount            | u    |  o         |                                    |
| URLs                   | as   |  o         |                                    |
+------------------------+------+------------+------------------------------------+

Container properties
====================

Each container may expose one or more resources via the Resources
property. Each resource typically represents a playlist, containing
references (often URLs) to the container's media items. Multiple
resources may be specified for a single container as the DMS may
support different playlist formats, e.g., M3U, DIDL-S, etc. The
properties of one of these resources may be duplicated directly in the
Container object itself. The algorithm for selecting this resource
is given in the section "Transcoding and org.gnome.UPnP.MediaItem2".
In MediaItem2 section also, you will find the definition for these properties
in Resources/Additional properties.

New Methods
===========

Eight new methods have been added. These methods are:

Extended versions of existing methods
-------------------------------------

- ListChildrenEx(u Offset, u Max, as Filter, s SortBy) -> aa{sv}
- ListItemsEx(u Offset, u Max, as Filter, s SortBy) -> aa{sv}
- ListContainersEx(u Offset, u Max, as Filter, s SortBy) -> aa{sv}
- SearchObjectsEx(s Query, u Offset, u Max, as Filter, s SortBy) -> (aa{sv}u)

The first four methods are identical in function and behaviour to
existing the MediaServer2Spec functions ListChildren, ListItems,
ListContainers, and SearchObjects, with the exception that they take
one extra parameter, and in the case of SearchObjectsEx, return an
extra result.

This extra parameter allows clients to specify a sort
order for the returned items.  It is a string that specifies a comma
separated list of PropertyNames, identifying the order in which
returned items should be sorted.  Each property name can be preceded
with a '-' or a '+' to indicate descending or ascending order
respectively.  An example, is "+Date,-DisplayName", which will sort
the return items first by date in ascending order and then by name in
descending order.  White space is not permitted in this string.

The return signature of SearchObjectsEx is (aa{sv}u).  Note the extra
integer return value after the dictionary of objects.  This integer
contains the total number of items matching the specified search as
opposed to the total number of objects contained in the returned dictionary
of objects.  These two values may differ if the client has used the
Offset and Max parameters to request a portion of the total
results returned, because for example its screen is only capable of
displaying 20 objects at any one time.  Knowing the total number of
objects that match a search is useful for applications.
It allows them to inform the user as to the total number of matches
and to correctly compute the scrollbars of the list displaying
the found items.

A small Python function is given below to demonstrate how these new
methods may be used.  This function accepts one parameter, a path to a
D-Bus container object, and it prints out the names of all the
children of that object in descending order.

.. code:: python

    def list_children(server_path):
        bus = dbus.SessionBus()
        container = dbus.Interface(bus.get_object(
                    'com.intel.dleyna-server', path),
                                            'org.gnome.UPnP.MediaContainer2')
        objects = container.ListChildrenEx(0, 0, ['DisplayName'], "-DisplayName")
        for item in objects:
            for key, value in item.iteritems():
                print '{0:<30}{1:<30}'.format(key, value)

.. compound::

    The output of this function might look something like this::

        DisplayName                   Videos
        DisplayName                   Pictures
        DisplayName                   Music
        DisplayName                   Files & Folders

    when the server_path parameter contains the path of a server object.


Methods with no direct counterpart in the original interface
------------------------------------------------------------
- Upload(s DisplayName, s FilePath) -> (u UploadId, o ObjectPath)
- CreateContainer(s DisplayName, s TypeEx, as ChildTypes) -> o ObjectPath
- CreateReference(o ObjectPath) -> o ObjectPath
- GetCompatibleResources(s protocol_info, as filter) -> a{sv}

The fifth new method, Upload, allows clients to upload a local file to
the org.gnome.UPnP.MediaContainer2 object upon which it was executed.
It is identical in function and behavior to
com.intel.dLeynaServer.MediaDevice.UploadToAnyContainer with the sole
exception that Upload allows the client to specify the server-side
container into which the file is to be uploaded whereas
UploadToAnyContainer leaves it up to the server to determine the most
suitable location for the file.

The CreateContainer method creates a new child container.
DisplayName is the name of the new container,
TypeEx is the extended type and ChildTypes is an array of extended
types that can be stored in the new folder, e.g., ['video','container'].
A special value of ['*'] indicates that no restriction is placed
on the types of objects that can be stored in the container.
The path of the newly created object is returned.

The CreateReference method creates a reference in the container to the object
identified by the ObjectPath parameter. This method returns the D-Bus path of
the newly created reference.  CreateReference is useful when creating
server side playlists.  CreateContainer can be used to create a new playlist,
and CreateReference can be used to add items to that playlist.

The GetCompatibleResources method: see description in MediaItem2.

Recommended Usage
-----------------

All of the list and search functions supported by dleyna-server-service's
implementation of org.gnome.UPnP.MediaContainer2 contain three
parameters that should be used to improve the efficiency and
responsiveness of applications built using dleyna-server-service.

The first two parameters of interest are offset and max.  These
parameters allow the client application to retrieve the contents of a
directory, or the results of a search, in chunks.  This is vital if
the client's user interface is limited to displaying a fixed number,
let's say 30, items on the screen at any one time.  Suppose the client
performs a search which has 2000 results.  If it passes the Search or
SearchEx method an offset and a max of 0, all of the results will be
returned to the client at once, even though it is only capable of
display 30 items at a time.  This will increase the memory
requirements of the client and reduce its responsiveness as it must
wait until all 2000 items have been received before it can update the
UI.  Also, if the user locates the item he is looking for in the first
page of items, a lot of network and IPC traffic will have been
generated in vain.  For these reasons, it is better to retrieve items
in chunks, as needed.

The amount of network and IPC traffic can be reduced further by
prudent use of the filter parameter.  This parameter is an array of
strings, each element of which corresponds to a property name.  If the
client invokes a function specifying a filter parameter that is set to
a single element array containing the string '*', dleyna-server-service
will include all the properties of all the requested objects in the
result.  However, often the client only needs to know a subset of
these properties.  A UI that displays results of a search might only
want to display the names and perhaps the dates of the items that
match the search.  Once the user has identified the item he is looking
for, the remaining properties for that item, and only that item, can
be retrieved.  As an example, consider the list_children function
above.  It requests that only the DisplayName of each of the
containers' children be returned.  Replacing ['DisplayName'] with
['*'] will generate a lot more output.

.. rubric:: Footnotes

.. [#f1] m/o indicates whether the property is optional or mandatory.
.. [#f2] A value of -1 for the srs-rt-retention-period capability denotes an infinite retention period. (What?!)


