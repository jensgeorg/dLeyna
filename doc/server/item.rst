================
The Item objects
================

Item objects represent a unique piece of media that can be downloaded
or streamed.  Each item object implements the
org.gnome.UPnP.MediaObject2 interface, which is described above, and
the org.gnome.UPnP.MediaItem2 interface that is documented in the
MediaServer2Spec [MS2S]_.  Although the org.gnome.UPnP.MediaItem2 is
documented elsewhere, there are some peculiarities of its
implementation in dleyna-server-service that merit further explanation.
These are described in the sections below.


*****************************************
Transcoding and org.gnome.UPnP.MediaItem2
*****************************************

As mentioned above, DMSs often provide different representations of
the same media file.  Separate sets of meta data, such as a mime type,
a URL, a size, a ColorDepth, etc., are associated with each
representation.  Unfortunately, it is difficult to represent this in
org.gnome.UPnP.MediaItem2.  Although, org.gnome.UPnP.MediaItem2,
allows multiple URLs to be specified, it only permits a single set of
meta data to be associated with each item.  For example, the MIMEType
property is a single string and not an array.  If multiple URLs were
associated with the same item, one would not know to which URL the
MIMEType property pertained.  For this reason, dleyna-server-service only
ever returns one element in the URLs property.  By default, the value
of this URL and all of the meta data properties that can change with a
representation (MIMEType, DLNAProfile, Size, Duration, Bitrate,
SampleRate, BitsPerSample, Width, Height, ColorDepth) refer to the
first representation returned by the DMS for this item.  However, the
client can change this behaviour by calling
com.intel.dLeynaServer.Manager.SetProtocolInfo.  If this is done,
these properties correspond to the first representation returned by
the server that is compatible with the specified protocol info.  If no
representations are compatible, these properties will not be present
in the item.


*********
Resources
*********

dleyna-server-service offers a second solution to the above problem that
may be useful in certain programming environments.  It implements a
non standard property called "Resources" which is an array of
dictionaries.  Each dictionary represents a separate representation of
the item and contains all the properties, including the MIMEType and
the URL that pertain to that representation.  This can be convenient
if you wish to display the media item in a web browser.  You can
simply create a video or an audio tag with one source for each element
in the resources array and then let the browser work out which one
suits it best.  The specification for the Resources property is given
in the table below:

+-----------------+------+------------+-------------------------------------------+
|     Name        | Type | m/o [#f1]_ |              Description                  |
+=================+======+============+===========================================+
| Resources       |aa{sv}|  m         | Returns the set of resources associated   |
|                 |      |            | with an item                              |
+-----------------+------+------------+-------------------------------------------+

The names of the properties included in this array are identical to
the names of the org.gnome.UPnP.MediaItem2 properties to which they
correspond, e.g., MIMEType, Size.
However, four additional properties are included.

Please note that if you want the resource properties to be included in the
results of a call to one of the List or the Search functions, and you
are explicitly filtering properties, you must include "Resources" in
the filter array.  The filter parameter also applies to the contents
of the returned resources.  So if you specify a filter of ["MIMEType",
"URL", "Resources"] the dictionaries in the resources array will contain
only these properties.

dLeyna-server defines four additional resource properties.  These have been
added to improve our DLNA support.

+-----------------+------+------------+------------------------------------------+
|     Name        | Type | m/o [#f1]_ |              Description                 |
+-----------------+------+------------+------------------------------------------+
| DLNAConversion  | a{sb}|  o         |  Indicates whether the content of the    |
|                 |      |            |  Resource is in original source format   |
|                 |      |            |  or if the content is transcoded.        |
|                 |      |            |  There is 1 key: Transcoded.             |
|                 |      |            |  See Reference [CIPARAM]_: DLNA.ORG_CI.  |
|                 |      |            |                                          |
|                 |      |            |  New in v0.0.2                           |
+-----------------+------+------------+------------------------------------------+
| DLNAFlags       | a{sb}|  o         |  A dictionary of boolean values          |
|                 |      |            |  indicating the values of the various    |
|                 |      |            |  operations supported by a resource.     |
|                 |      |            |  There are 12 keys:                      |
|                 |      |            |  SenderPaced, TimeBased, ByteBased,      |
|                 |      |            |  PlayContainer, S0Increase, SNIncrease   |
|                 |      |            |  RTSPPause, StreamingTM, InteractiveTM   |
|                 |      |            |  BackgroundTM, ConnectionStall, DLNA_V15 |
|                 |      |            |  See [FLAGSPARAM]_: DLNA.ORG_FLAGS.      |
|                 |      |            |                                          |
|                 |      |            |  New in v0.0.2                           |
+-----------------+------+------------+------------------------------------------+
| DLNAOperation   | a{sb}|  o         |  A dictionary of boolean values          |
|                 |      |            |  indicating the values of the various    |
|                 |      |            |  DLNA Operation attributes. There are 2  |
|                 |      |            |  keys: RangeSeek and TimeSeek.           |
|                 |      |            |  See [OPPARAM]_: DLNA.ORG_OP.            |
|                 |      |            |                                          |
|                 |      |            |  New in v0.0.2                           |
+-----------------+------+------------+------------------------------------------+
| UpdateCount     |  u   |  o         |  Contains the number of times the        |
|                 |      |            |  implementation detects that a change    |
|                 |      |            |  was made to the resource.               |
|                 |      |            |                                          |
|                 |      |            |  New in v0.0.2                           |
+-----------------+------+------------+------------------------------------------+


******************
Additional Methods
******************

A third option is provided to make the lives of DMC authors easier.
In a DMC, the best resource is defined not by the local device but by
the capabilities of the renderer chosen by the user to play the given
item.  Once the user selects a file to play and a renderer upon which
to play it, the DMC needs to retrieve the renderer's Sink
ProtocolInfo.  It can then pass this data directly to a new function
that dleyna-server-service adds to the org.gnome.UPnP.MediaItem2
interface, called GetCompatibleResource.  GetCompatibleResource
returns a dictionary of properties that corresponds to the item
representation that best matches a specified protocol info.  The
signature of GetCompatibleResources is given below:

GetCompatibleResources(s protocol_info, as filter) -> a{sv}
-----------------------------------------------------------

    The first argument is a comma separated list of protocol info values,
    as described above.  The second argument is an array of properties to
    be included in the returned dictionary.  The format and the behaviour
    of this array is identical to the filter argument passed to the Search
    and List functions.

*********************
Additional Properties
*********************

Dleyna-server-service defines some additional properties for
org.gnome.UPnP.MediaItem2. These properties are described in the table
below:

+-----------------+------+------------+-------------------------------------------+
|     Name        | Type | m/o [#f1]_ |              Description                  |
+=================+======+============+===========================================+
| AlbumArtURL     |  s   |  o         | Contains the URL of the album art         |
|                 |      |            | associated with an item                   |
+-----------------+------+------------+-------------------------------------------+
| RefPath         |  o   |  o         | The presence of this property indicates   |
|                 |      |            | that the item is a reference item that    |
|                 |      |            | references another item located           |
|                 |      |            | elsewhere in the DMS's directory          |
|                 |      |            | structure. The value of this property     |
|                 |      |            | is the path of the referenced item.       |
+-----------------+------+------------+-------------------------------------------+
| Artists         |  o   |  m         | An array of all the artists who           |
|                 |      |            | contributed towards the creation of the   |
|                 |      |            | item. The array will be empty if no       |
|                 |      |            | artists are associated with the item.     |
+-----------------+------+------------+-------------------------------------------+
| DLNAConversion  | a{sb}|  o         | These four properties are copied from the |
+-----------------+------+------------+ currently selected resource into the      |
| DLNAFlags       | a{sb} |  o        | org.gnome.UPnP.MediaItem2 object.         |
+-----------------+------+------------+                                           |
| DLNAOperation   | a{sb} |  o        |                                           |
+-----------------+------+------------+                                           |
| UpdateCount     |  u    |  o        |                                           |
+-----------------+------+------------+-------------------------------------------+

dleyna-server-service does not implement the
org.gnome.UPnP.MediaItem2.AlbumArt property for the same reasons that
it does not implement the org.gnome.UPnP.MediaContainer2.Icon
property. However, it does provide an alternative method of retrieving
the album art associated with media content. It does this through a
new property called AlbumArtURL described in the table above that
exposes the URL of the album art directly.


************************
Unimplemented Properties
************************

dleyna-server-service does not support the following MediaServer2Spec
properties:

- PixelWidth
- PixelHeight
- Thumbnail
- AlbumArt

However, as these properties are optional, dleyna-server-service's
failure to support them does not affect its compatibility with
MediaServer2Spec.

.. rubric:: Footnotes

.. [#f1] m/o indicates whether the property is optional or mandatory.

