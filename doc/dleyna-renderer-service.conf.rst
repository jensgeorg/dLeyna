.. SPDX-License-Identifier: LGPL-2.1-or-later
.. dleyna-renderer-service.conf(5):

============================
dleyna-renderer-service.conf
============================

----------------------------------------------
Configuration file for dleyna-renderer-service
----------------------------------------------

:Manual section: 5

SYNOPSIS
========

    - ``/etc/dleyna-renderer-service.conf``
    - ``$XDG_CONFIG_HOME/dleyna-renderer-service.conf``

DESCRIPTION
===========

    The ``dleyna-renderer-service`` program provides the
    *com.intel.dleyna-renderer* name on the session message bus.
    It is a service for discovering and manipulating DLNA Digial Media Renderers
    (or DMRs).

    Users or administrators should never need to start this daemon as it will
    be automatically started by dbus-daemon whenever a process sends a D-Bus message
    to the *com.intel.dleyna-renderer* name on the session bus.

    This file can be used to configure the behavior of the ``dleyna-renderer-service`` program

FILE FORMAT
===========

    The configuration file format is the so-called key file (sort of
    ini-style) format. It consists of sections (or groups) of key-value
    Lines beginning with a '#' and blank lines are considered comments.
    Sections are started by a header line containing the section enclosed
    in '[' and ']', and ended implicitly by the start of the next section
    or the end of the file. Each key-value pair must be contained in a
    section.

GENERAL SECTION
===============

    connector-name=dbus
      The name of the inter-process communication method to be used.
      The only supported value is *dbus*

    never-quit=false|true
      Whether the process should stay alive forever or quit when the
      last client disconnects. The default value is *false*.

    port
      Source port for Simple Service Discovery Protocol (or SSDP)
      messages. The default value is *0* which
      means that a random available port will be used.

    push-host-port
      Port for push host file server. The default value is
      *0* which means that a random available port will be used.

LOG SECTION
===========

    log-type=0|1|2
      The logging method to be used. Three technologies are supported.
      0 for syslog, 1 for GLib, and 2 for file. The default value is
      *0*.

    log-level=0|1|2|...|8
      Comma-separated list of logging levels. The levels are 1 for
      critical, 2 for error, 3 for warning, 4 for message, 5 for info
      and 6 for debug. 0 excludes all levels, 7 is a combination of
      critical, error and info, and 8 includes all levels.

      The default value is *7*.

NETF SECTION
============

    netf-enabled=true|false
      Whether network filtering should be enabled or not. The default
      values is *false*.

    netf-list
       Comma-separated list of interface names, service set identifiers
       (or SSIDs), and Internet Protocol (or IP) addresses. If network
       filtering is requested but the list is empty, then filtering
       remains disabled.

       The list is empty by default.

BUGS
====

Please send bug reports to either the distribution bug tracker
or the upstream bug tracker at https://gitlab.gnome.org/World/dLeyna/issues

SEE ALSO
========

    dleyna-renderer-service(1)

