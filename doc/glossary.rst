========
Glossary
========

.. glossary::
   two-box-model
        The two-box-model is one of the common DLNA use-cases. It consists of two
        devices:

        - DMS
        - DMP

        This combines the two devices DMR and control point into the single DMP device

   two-box-push+model
        No DMS, only CP and DMR

   three-box-model
        The three-box-model is one of the two common DLNA use-cases. It consists of three
        devices:

        - DMS
        - DMR
        - Control Point

        For a "real world" comparison, think of the DMR as a dumb TV, the control point
        as your universal remote control and the DMS as your media playback device (VCR,
        DVD player, Blueray, ...)

   DMS
        Digital Media Server. A digital media server is an :term:`UPnP` device defined
        by DLNA that implements a pre-defined set of devices and services for
        hosting media files on a network.

   DMR
        Digital Media Renderer. A digital media renderer is an UPnP MediaRenderer:1
        device defined by DLNA that contains a mandatory set of services. It usually has
        no capability of its own to access a :term:`DMS` (see also :term:`DMP`).
        It is usually instructed by a control point to render media files from
        a server

   DMP
        Digital Media Player. A digital media player is a software that combines
        the media ren

   UPnP
        Universal Plug and Play. A set of network protocols and guidelines for
        automatic device discovery and interoperability. UPnP covers a broad range
        of use-cases, the most prominent ones are internet connection manipulation
        and media sharing.

   DLNA 
        Digital Living Network Alliance. UPnP used to be a very wide specification.
        It allowed devices that both correctly claim to be UPnP comatible not to be
        compatible with each other at all. DLNA aimed to solve this by narrowing the
        UPnP specification and mandating a baseline of to be supported media codecs.

   LAN
        Local Area Network. Usually UPnP and DLNA only work on your local network.

   D-Bus
        Local IPC server and standard used in many open source installations

