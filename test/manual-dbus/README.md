Manual tests
============

This folder contains a number of manual tests or helper tools for manual tests of dLeyna's DBus services

Renderer
--------

* lost_client_renderer_test.py - Connects to the first renderer its finds and immediately drops the connection to the
  DBus service
* stress-test-renderer.py - Connects to the first server it finds and repeatedly requests all properties, immediately
  cancelling the request

Server
------
* lost_client_test.py - Connects to the first server it finds and immediately drops the connection to the DBus service
* monitor_changed.py - Connects to the server service and dumps all Changed signals
* monitor_contents_changed.py - Connects to the server service and dumps all signals related to server content changes
* monitor_upload_update.py - Connects to the server service and tracks all upload progress notifications
* stress_test.py - Connects to the first server it finds and repeatedly requests all properties, immediately cancelling
  the request
