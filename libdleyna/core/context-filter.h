/*
 * dLeyna
 *
 * Copyright (C) 2012-2017 Intel Corporation. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU Lesser General Public License,
 * version 2.1, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin St - Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Ludovic Ferrandis <ludovic.ferrandis@intel.com>
 *
 */

#pragma once

#include <glib.h>
#include <libgupnp/gupnp-context-filter.h>

typedef struct dleyna_context_filter_t_ dleyna_context_filter_t;

dleyna_context_filter_t *dleyna_context_filter_new(GUPnPContextFilter *gupnp_wl);

void dleyna_context_filter_delete(dleyna_context_filter_t *wl);

void dleyna_context_filter_enable(dleyna_context_filter_t *wl, gboolean enabled);

void dleyna_context_filter_add_entries(dleyna_context_filter_t *wl, GVariant *entries);

void dleyna_context_filter_remove_entries(dleyna_context_filter_t *wl,
				      GVariant *entries);

void dleyna_context_filter_clear(dleyna_context_filter_t *wl);
